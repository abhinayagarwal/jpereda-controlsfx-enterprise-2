package org.controlsfx.control.tableview2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Arrays;
import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Separator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.controlsfx.control.action.ActionUtils;
import org.controlsfx.control.tableview2.actions.ColumnFreezeAction;
import org.controlsfx.control.tableview2.actions.RowFreezeAction;
import org.controlsfx.control.tableview2.cell.ComboBox2TableCell;
import org.controlsfx.control.tableview2.cell.TextField2TableCell;

public class Test2TVvsTV2 extends Application {
    
    private final ObservableList<Person> data = Person.generateData(100);
    private final ObservableList<Person> dataOrig = Person.generateData(100);
    
    private final TableView2<Person> table = new TableView2<>();
    private final TableColumn2<Person, String> firstName = new TableColumn2<>("First Name");
    private final TableColumn2<Person, String> lastName = new TableColumn2<>("Last Name");
    private final TableColumn2<Person, String> city = new TableColumn2<>("City");
    private final TableColumn2<Person, LocalDate> birthday = new TableColumn2<>("Birthday");
    private final TableColumn2<Person, Boolean> active = new TableColumn2<>("Active");
    
    private final TableView<Person> tableOrig = new TableView<>();
    private final TableColumn<Person, String> firstNameOrig = new TableColumn<>("First Name");
    private final TableColumn<Person, String> lastNameOrig = new TableColumn<>("Last Name");
    private final TableColumn<Person, String> cityOrig = new TableColumn<>("City");
    private final TableColumn<Person, LocalDate> birthdayOrig = new TableColumn<>("Birthday");
    private final TableColumn<Person, Boolean> activeOrig = new TableColumn<>("Active");
    
    private final VBox controls = new VBox(10);
    
    private HBox boxFirstName, boxLastName, boxBirthday;
    
    @Override
    public void start(Stage primaryStage) {
        setTableView();
        setTableView2();
        setControls();
        final HBox hBox = new HBox(50, tableOrig, controls, table);
        HBox.setHgrow(tableOrig, Priority.ALWAYS);
        HBox.setHgrow(table, Priority.ALWAYS);
        hBox.setPadding(new Insets(20));
        Scene scene = new Scene(hBox, 1200, 500);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        
       
        table.getFixedRows().addAll(0, 1, 2);
//        table.getFixedColumns().add(fullNameColumn);
//        table.getFixedColumns().addAll(firstName, lastName);
    }
    
    private void setTableView2() {
        firstName.setCellValueFactory(p -> p.getValue().firstNameProperty());
//        firstName.setCellFactory(TextFieldTableCell.forTableColumn());
        firstName.setCellFactory(ComboBox2TableCell.forTableColumn("Name 1", "Name 2", "Name 3", "Name 4"));
        firstName.setPrefWidth(110);
        
        lastName.setCellValueFactory(p -> p.getValue().lastNameProperty());
//        lastName.setCellFactory(TextFieldTableCell.forTableColumn());
        lastName.setCellFactory(TextField2TableCell.forTableColumn());
        lastName.setPrefWidth(130);
        
        city.setCellValueFactory(p -> p.getValue().cityProperty());
//        city.setCellFactory(TextFieldTableCell.forTableColumn());
        city.setCellFactory(p -> new TableCell<Person, String>() {
            
            private final Button button;
            {
                button = new Button();
            }
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty); 
                if (item != null && ! empty) {
                    button.setText(item);
                    if (getIndex() % 5 == 0) {
                        setStyle("-fx-font-size: 2em;");
                    } else {
                        setStyle("-fx-font-size: 1em;");
                    }
                    setText(null);
                    setGraphic(button);
                } else {
                    setText(null);
                    setGraphic(null);
                    setStyle(null);
                }
            }
            
        });

        birthday.setCellValueFactory(p -> p.getValue().birthdayProperty());
        birthday.setPrefWidth(100);
        birthday.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate date) {
                if (date == null) {
                    return "" ;
                } 
                return DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(date);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.now();
            }
            
        }));
        
        active.setText("Active");
        active.setCellValueFactory(p -> p.getValue().activeProperty());
        active.setCellFactory(CheckBoxTableCell.forTableColumn(active));
        active.setPrefWidth(60);
        
        table.setItems(data);
        TableColumn fullNameColumn = new TableColumn("Full Name");
        fullNameColumn.getColumns().addAll(firstName, lastName);
        table.getColumns().addAll(fullNameColumn, city, birthday, active);
//        table.getColumns().addAll(firstName, lastName, city, birthday, active);
        
        ContextMenu cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFreezeAction(table, fullNameColumn)));
        fullNameColumn.setContextMenu(cm);
        
        Button buttonFirstName = new Button("Resize");
        buttonFirstName.setOnAction(e -> {
            table.resizeColumn(firstName, 150);
        });
        boxFirstName = new HBox(10, buttonFirstName);
        boxFirstName.setAlignment(Pos.CENTER);
        
        Label labelLastName = new Label("1:");
        labelLastName.textProperty().bind(Bindings.createStringBinding(() -> 
            "1: " + table.getItems().stream().filter(t -> t.getLastName().contains("1")).count(), table.getItems()));
        boxLastName = new HBox(10, labelLastName);
        boxLastName.setAlignment(Pos.CENTER);
        
        cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFreezeAction(table, city)));
        city.setContextMenu(cm);
        
        cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFreezeAction(table, birthday), ActionUtils.ACTION_SEPARATOR));
        birthday.setContextMenu(cm);
        
        Button buttonBirthday = new Button("X");
        buttonBirthday.setOnAction(e -> 
            table.lookupAll(".south").forEach(h -> h.getStyleClass().remove("south")));
        Button cancelBirthday = new Button("S");
        cancelBirthday.setOnAction(e -> {
            table.lookupAll(".south-header").forEach(h -> {
                if (! h.getStyleClass().contains("south")) {
                    h.getStyleClass().add("south");
                }
            });
            table.lookupAll(".leaf-header").forEach(h -> {
                if (! h.getStyleClass().contains("south")) {
                    h.getStyleClass().add("south");
                }
            });
        });
        boxBirthday = new HBox(10, buttonBirthday, cancelBirthday);
        boxBirthday.setAlignment(Pos.CENTER);
        
        cm = ActionUtils.createContextMenu(Arrays.asList(new ColumnFreezeAction(table, active)));
        active.setContextMenu(cm);
        
        table.setRowHeaderContextMenuFactory((i, person) -> {
            ContextMenu rowCM = ActionUtils.createContextMenu(Arrays.asList(new RowFreezeAction(table, i), ActionUtils.ACTION_SEPARATOR));
            
            final MenuItem menuItem = new MenuItem("Remove  " + person.getFirstName());
            menuItem.setOnAction(e -> {
                if (i >= 0) {
                    final ObservableList<Person> items = table.getItems();
                    if (items instanceof SortedList) {
                        int sourceIndex = ((SortedList<Person>) items).getSourceIndexFor(data, i);
                        data.remove(sourceIndex);
                    } else {
                        data.remove(i.intValue());
                    }
                }
            });
            final MenuItem menuItemAdd = new MenuItem("Add new Person");
            menuItemAdd.setOnAction(e -> {
                data.add(new Person());
            });
            rowCM.getItems().addAll(menuItem, menuItemAdd);
            return rowCM; 
        });
        
        table.getFixedColumns().add(city);
    }
    
    private void setTableView() {
        firstNameOrig.setCellValueFactory(p -> p.getValue().firstNameProperty());
        //firstNameOrig.setCellFactory(TextFieldTableCell.forTableColumn());
        firstNameOrig.setCellFactory(p -> {
            ComboBoxTableCell<Person, String> cell = new ComboBoxTableCell<>("Name 1", "Name 2", "Name 3", "Name 4");
            cell.setComboBoxEditable(true);
            return cell;
        });
        firstNameOrig.setPrefWidth(110);
        
        lastNameOrig.setCellValueFactory(p -> p.getValue().lastNameProperty());
        lastNameOrig.setCellFactory(TextFieldTableCell.forTableColumn());
        lastNameOrig.setPrefWidth(130);
        
        cityOrig.setCellValueFactory(p -> p.getValue().cityProperty());
        cityOrig.setCellFactory(TextFieldTableCell.forTableColumn());
//        cityOrig.setStyle("-fx-font-size: 2em;");
        
        birthdayOrig.setCellValueFactory(p -> p.getValue().birthdayProperty());
        birthdayOrig.setPrefWidth(100);
        birthdayOrig.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<LocalDate>() {
            @Override
            public String toString(LocalDate date) {
                if (date == null) {
                    return "" ;
                } 
                return DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(date);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.now();
            }
            
        }));
        
        activeOrig.setText("Active");
        activeOrig.setCellValueFactory(p -> p.getValue().activeProperty());
        activeOrig.setCellFactory(CheckBoxTableCell.forTableColumn(activeOrig));
        activeOrig.setPrefWidth(60);
        
        tableOrig.setItems(dataOrig);
        TableColumn column = new TableColumn("Full Name");
        column.getColumns().addAll(firstNameOrig, lastNameOrig);
        tableOrig.getColumns().addAll(column, cityOrig, birthdayOrig, activeOrig);
        
    }
    
    private void setControls() {
    
        CheckBox tableEditionEnabled = new CheckBox("Table Edition Enabled");
        tableEditionEnabled.selectedProperty().addListener((obs, ov, nv) -> {
            table.setEditable(nv);
            tableOrig.setEditable(nv);
        });
        tableEditionEnabled.setSelected(true);
        
        CheckBox columnsEditionEnabled = new CheckBox("Columns Edition Enabled");
        columnsEditionEnabled.selectedProperty().addListener((obs, ov, nv) -> {
            table.getVisibleLeafColumns().forEach(column -> column.setEditable(nv));
            tableOrig.getVisibleLeafColumns().forEach(column -> column.setEditable(nv));
        });
        columnsEditionEnabled.setSelected(true);
        
        CheckBox cellSelectionEnabled = new CheckBox("Cell Selection Enabled");
        cellSelectionEnabled.selectedProperty().addListener((obs, ov, nv) -> {
            table.getSelectionModel().setCellSelectionEnabled(nv);
            tableOrig.getSelectionModel().setCellSelectionEnabled(nv);
        });
        
        CheckBox selectionMultiple = new CheckBox("Selection Multiple");
        selectionMultiple.selectedProperty().addListener((obs, ov, nv) -> {
            table.getSelectionModel().setSelectionMode(nv ? SelectionMode.MULTIPLE : SelectionMode.SINGLE);
            tableOrig.getSelectionModel().setSelectionMode(nv ? SelectionMode.MULTIPLE : SelectionMode.SINGLE);
        });
        
        CheckBox columnPolicy = new CheckBox("Column Policy Constrained");
        columnPolicy.selectedProperty().addListener((obs, ov, nv) -> {
            table.setColumnResizePolicy(nv ? TableView.CONSTRAINED_RESIZE_POLICY : TableView.UNCONSTRAINED_RESIZE_POLICY);
            tableOrig.setColumnResizePolicy(nv ? TableView.CONSTRAINED_RESIZE_POLICY : TableView.UNCONSTRAINED_RESIZE_POLICY);
        });
        
        CheckBox fixedCellSize = new CheckBox("Set Fixed Cell Size");
        fixedCellSize.selectedProperty().addListener((obs, ov, nv) -> {
            table.setFixedCellSize(nv ? 40 : 0);
            tableOrig.setFixedCellSize(nv ? 40 : 0);
        });
        
        CheckBox showTableMenuButton = new CheckBox("Show Table Menu Button");
        showTableMenuButton.selectedProperty().addListener((obs, ov, nv) -> {
            table.setTableMenuButtonVisible(nv);
            tableOrig.setTableMenuButtonVisible(nv);
        });
        
        CheckBox showData = new CheckBox("Show Data");
        showData.setSelected(true);
        showData.selectedProperty().addListener((obs, ov, nv) -> {
            table.setItems(nv ? data : null);
            tableOrig.setItems(nv ? dataOrig : null);
        });
        
        CheckBox sortedList = new CheckBox("Use SortedList");
        sortedList.selectedProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                FilteredList<Person> filteredData = new FilteredList<>(data, p -> true);
                SortedList<Person> sortedData = new SortedList<>(filteredData);
                sortedData.comparatorProperty().bind(table.comparatorProperty());
                table.setItems(sortedData);
            } else {
                table.setItems(data);
            }
            
            if (nv) {
                FilteredList<Person> filteredData = new FilteredList<>(dataOrig, p -> true);
                SortedList<Person> sortedData = new SortedList<>(filteredData);
                sortedData.comparatorProperty().bind(tableOrig.comparatorProperty());
                tableOrig.setItems(sortedData);
            } else {
                tableOrig.setItems(dataOrig);
            }
        });
        
        controls.getChildren().addAll(new Label("Common API"), new Separator(), 
                tableEditionEnabled, columnsEditionEnabled, 
                cellSelectionEnabled, selectionMultiple, columnPolicy, fixedCellSize, showTableMenuButton, showData, sortedList);
        
        CheckBox showRowHeader = new CheckBox("Show Row Header");
        showRowHeader.selectedProperty().addListener((obs, ov, nv) -> {
            table.setRowHeaderVisible(nv);
        });
        
        CheckBox columnFixing = new CheckBox("Column Fixing Enabled");
        columnFixing.setSelected(true);
        columnFixing.selectedProperty().addListener((obs, ov, nv) -> {
            table.setColumnFixingEnabled(nv);
        });
        
        CheckBox rowFixing = new CheckBox("Row Fixing Enabled");
        rowFixing.setSelected(true);
        rowFixing.selectedProperty().addListener((obs, ov, nv) -> {
            //table.setRowFixingEnabled(nv);
            table.rowFactoryProperty().unbind();
            table.setRowFactory(p -> new TableRow<Person>() {
                @Override
                protected void updateItem(Person item, boolean empty) {
                    super.updateItem(item, empty); 
                }
                
            });
        });
        
        CheckBox southFilter = new CheckBox("Use SouthFilter");
        southFilter.selectedProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                firstName.setSouthNode(boxFirstName);
                lastName.setSouthNode(boxLastName);
                birthday.setSouthNode(boxBirthday);
            } else {
                firstName.setSouthNode(null);
                lastName.setSouthNode(null);
                birthday.setSouthNode(null);
            }
        });
        
        CheckBox rowFactory = new CheckBox("Use Row Header Factory");
        rowFactory.selectedProperty().addListener((obs, ov, nv) -> {
            if (nv) {
                TableColumn2<Person, Number> tc = new TableColumn2<>();
                tc.setContextMenu(new ContextMenu(new MenuItem("Corner")));
                tc.setGraphic(new Rectangle(20, 20, Color.ORANGE));
                
                Button cancelButton = new Button("X");
                HBox box = new HBox(cancelButton);
                box.setAlignment(Pos.CENTER);
                tc.setSouthNode(box);
                
                tc.setCellValueFactory(p -> p.getValue().getTotalSum());
                tc.setCellFactory(p -> new TableCell<Person, Number>() {
                    private final HBox box;
                    private final Circle circle;
                    private final Label label;
                    {
                        circle = new Circle(5); 
                        label = new Label(); 
                        box = new HBox(10, circle, label); 
                    }
                    
                    @Override
                    protected void updateItem(Number item, boolean empty) {
                        super.updateItem(item, empty); 
                        if (item != null && ! empty) {
                            setText(null);
                            circle.setFill(getIndex() % 5 == 0 ? Color.RED : Color.BLUE);
                            label.setText("" + table.getItems().get(getIndex()).getBirthday().getYear() + " " + String.valueOf(item));
                            box.setAlignment(Pos.CENTER);
                            setGraphic(box);
                        } else {
                            setText(null);
                            setGraphic(null);
                        }
                    }

                });
                table.setRowHeader(tc);
            } else {
                table.setRowHeader(null);
            }

            table.setRowHeaderWidth(nv ? 100 : 40);
        });
        
        controls.getChildren().addAll(new Label("TableView2 API"), new Separator(), showRowHeader, columnFixing, rowFixing, 
                southFilter, rowFactory);
        controls.setAlignment(Pos.CENTER_LEFT);
        controls.setMinWidth(200);
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}