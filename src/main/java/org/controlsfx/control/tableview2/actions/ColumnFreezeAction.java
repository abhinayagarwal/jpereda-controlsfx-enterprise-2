/**
 * Copyright (c) 2013, 2018 ControlsFX
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *     * Neither the name of ControlsFX, any associated website, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL CONTROLSFX BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.controlsfx.control.tableview2.actions;

import static impl.org.controlsfx.i18n.Localization.asKey;
import static impl.org.controlsfx.i18n.Localization.localize;
import javafx.beans.Observable;
import javafx.scene.Node;
import javafx.scene.control.TableColumn;
import org.controlsfx.control.action.Action;
import org.controlsfx.control.action.ActionCheck;
import org.controlsfx.control.action.ActionUtils;
import org.controlsfx.control.tableview2.TableView2;

/**
 * A custom action that can be added the ContextMenu of a 
 * column header, allowing the user to freeze or unfreeze the column.
 * 
 * This action has to be bound to a CheckMenuItem node.
 * 
 * @see ActionUtils#createContextMenu(java.util.Collection) 
 */
@ActionCheck
public class ColumnFreezeAction extends Action {

    private final TableView2 tableView;
        
    public ColumnFreezeAction(TableView2 tableView, TableColumn column) {
        this(tableView, column, localize(asKey("tableview2.column.menu.frozen")));
    }

    public ColumnFreezeAction(TableView2 tableView, TableColumn column, String name) {
        this(tableView, column, name, null);
    }

    public ColumnFreezeAction(TableView2 tableView, TableColumn column, String name, Node image) {
        super(name);
        this.tableView = tableView;

        setGraphic(image);
        
        if (tableView != null && column != null) {
            disabledProperty().bind(tableView.columnFixingEnabledProperty().not()
                    .or(column.parentColumnProperty().isNotNull()));

            tableView.getFixedColumns().addListener((Observable o) -> {
                setSelected(isFixedColumn(column));
            });
            setSelected(isFixedColumn(column));
        
            setEventHandler(e -> {
                if (! tableView.getFixedColumns().contains(column)) {
                    tableView.getFixedColumns().add(column);
                } else {
                    tableView.getFixedColumns().remove(column);
                }
            });
        }
    }

    @Override 
    public String toString() {
        return getText();
    }
    
    private boolean isFixedColumn(TableColumn column) {
        while (column.getParentColumn() != null) {
            column = (TableColumn) column.getParentColumn();
        }
        return tableView.getFixedColumns().contains(column);
    } 
}
