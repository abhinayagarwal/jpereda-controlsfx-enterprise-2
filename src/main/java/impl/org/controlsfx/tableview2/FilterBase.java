/**
 * Copyright (c) 2016, 2017 ControlsFX
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *     * Neither the name of ControlsFX, any associated website, nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL CONTROLSFX BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package impl.org.controlsfx.tableview2;

import javafx.scene.control.CheckBox;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import org.controlsfx.control.tableview2.TableView2;

/**
 * A simple implementation of the {@link Filter}. 
 * 
 * TODO: implement filter
 */
class FilterBase implements Filter {

    private final TableView2<?> tableView;
    private MenuButton menuButton;

    /**
     * Constructor for the Filter indicating on which column it's applied on.
     *
     * @param tableView the {@link TableView2}
     * @param column the number of column
     */
    public FilterBase(TableView2<?> tableView, int column) {
        this.tableView = tableView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MenuButton getMenuButton() {
        if (menuButton == null) {
            menuButton = new MenuButton();
            menuButton.getStyleClass().add("filter-menu-button");

            menuButton.showingProperty().addListener((obs, oldValue, newValue) -> {
                if (newValue) {
                    addMenuItems();
                }
            });
        }
        return menuButton;
    }

    private void addMenuItems() {
        if (menuButton.getItems().isEmpty()) {
            ListView<String> listView = new ListView<>();
            listView.setCellFactory(param -> new ListCell<String>() {
                @Override
                public void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    setText(item);
                    if (item != null) {
                        CheckBox checkBox = new CheckBox();
                        setGraphic(checkBox);
                    }
                }
            });

            CustomMenuItem customMenuItem = new CustomMenuItem(listView);
            customMenuItem.setHideOnClick(false);
            menuButton.getItems().addAll(customMenuItem);
        }

    }

}
